import os
import shutil
import requests
import pandas as pd
from utils.utils import *
from database.config import engine


class App:

    @staticmethod
    def start():
        raw_data_path = 'C:/Users/Gabriel/Documents/trybe-challenge/data/raw_data'
        selected_data_path = 'C:/Users/Gabriel/Documents/trybe-challenge/data/selected_data'
        report = 'Enrollment'

        locations = list()
        ibge_api_url = 'https://servicodados.ibge.gov.br/api/v1/localidades/municipios/'
        response = requests.get(ibge_api_url)
        results = response.json()
        for result in results:
            location = {'id': result['id'], 'nome': result['nome']}
            locations.append(location)

        zip_file = return_zipfile_object(raw_data_path)
        list_of_files_inside_zip_file = return_list_of_file_inside_zip_file(zip_file)
        show_files_inside_zip_file(list_of_files_inside_zip_file)
        extract_specific_file_based_on_name_or_prefix(zip_file, selected_data_path, prefix='matricula_')

        if os.path.isdir(os.path.join(selected_data_path, report, 'microdados_educacao_basica_2020/DADOS/')):
            files = os.listdir(os.path.join(selected_data_path, report, 'microdados_educacao_basica_2020/DADOS/'))
            for file in files:
                if os.path.isfile(os.path.join(selected_data_path, report, file)):
                    pass
                else:
                    shutil.move(os.path.join(selected_data_path, report, f'microdados_educacao_basica_2020/DADOS/{file}'), os.path.join(selected_data_path, report))
            shutil.rmtree(os.path.join(selected_data_path, report, f'microdados_educacao_basica_2020'))
        else:
            pass

        csv_files_list = return_list_of_files_inside_specific_folder(selected_data_path)

        for csv_file in csv_files_list:
            print(f'Loading {csv_file}...')
            if 'amostra' in csv_file:
                pass
            else:
                df = transform_file_into_pandas_dataframe(selected_data_path, csv_file, size=1000000)
                df['TP_SEXO'] = df['TP_SEXO'].replace(to_replace=[1, 2],
                                                    value=['M', 'F'])
                df['TP_COR_RACA'] = df['TP_COR_RACA'].replace(to_replace=[0, 1, 2, 3, 4, 5],
                                                            value=['Não declarada', 'Branca', 'Preta', 'Parda', 'Amarela', 'Indígena'])

                df['NO_MUNICIPIO'] = df['CO_MUNICIPIO'].replace(to_replace=[location['id'] for location in locations],
                                                                value=[location['nome'] for location in locations])
                df = df[['ID_ALUNO',
                        'ID_MATRICULA',
                        'ID_TURMA',
                        'NU_ANO_CENSO',
                        'CO_UF',
                        'CO_MUNICIPIO',
                        'NO_MUNICIPIO',
                        'NU_IDADE_REFERENCIA',
                        'TP_SEXO',
                        'TP_COR_RACA',
                        'TP_NACIONALIDADE',
                        'TP_ETAPA_ENSINO']]
                if csv_file.split('.')[0] == 'matricula_co':
                    df['CO_UF'] = df['CO_UF'].replace(to_replace=[50, 51, 52, 53], value=['MS', 'MT', 'GO', 'DF'])
                elif csv_file.split('.')[0] == 'matricula_nordeste':
                    df['CO_UF'] = df['CO_UF'].replace(to_replace=[21, 22, 23, 24, 25, 26, 27, 28, 29], value=['MA', 'PI', 'CE', 'RN', 'PB', 'PE', 'AL', 'SE', 'BA'])
                elif csv_file.split('.')[0] == 'matricula_norte':
                    df['CO_UF'] = df['CO_UF'].replace(to_replace=[11, 12, 13, 14, 15, 16, 17], value=['RO', 'AC', 'AM', 'RR', 'PA', 'AP', 'TO'])
                elif csv_file.split('.')[0] == 'matricula_sudeste':
                    df['CO_UF'] = df['CO_UF'].replace(to_replace=[31, 32, 33, 35], value=['MG', 'ES', 'RJ', 'SP'])
                elif csv_file.split('.')[0] == 'matricula_sul':
                    df['CO_UF'] = df['CO_UF'].replace(to_replace=[41, 42, 43], value=['PR', 'SC', 'RS'])
                send_pandas_dataframe_to_database(df, engine, csv_file.split('.')[0], index=False)
                print(f'{csv_file.split(".")[0]} loaded successfully!\n')
