SELECT NU_ANO_CENSO 'Censo',
       TP_COR_RACA 'Cor/Raça', 
       CO_UF 'Estado', 
       COUNT(ID_ALUNO) 'Nº de Alunos por Cor em cada Estado',
       (SELECT COUNT(ID_ALUNO) FROM trybe_db.matriculas) AS 'Número total de alunos matriculados',
       CONCAT(ROUND((COUNT(ID_ALUNO) * 100) / (SELECT COUNT(ID_ALUNO) FROM trybe_db.matriculas), 1), '%') 'Nº de Alunos por Cor em cada Estado (%)'  
       FROM trybe_db.matriculas GROUP BY TP_COR_RACA, CO_UF
       ORDER BY COUNT(ID_ALUNO) DESC;
