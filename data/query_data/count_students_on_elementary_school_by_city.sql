SELECT NU_ANO_CENSO 'Censo',
       NO_MUNICIPIO 'Cidade',
       CO_UF 'Estado',
       COUNT(ID_ALUNO) 'Nº de Alunos no EF de 9 anos no 9º ano ',
	   (SELECT COUNT(ID_ALUNO) FROM trybe_db.matriculas) AS 'Número total de alunos matriculados',
       CONCAT(ROUND((COUNT(ID_ALUNO) * 100) / (SELECT COUNT(ID_ALUNO) FROM trybe_db.matriculas), 1), '%') 'Nº de Alunos no EF de 9 anos no 9º ano (%)'
       FROM trybe_db.matriculas WHERE TP_ETAPA_ENSINO=41 GROUP BY CO_MUNICIPIO ORDER BY COUNT(ID_ALUNO) DESC LIMIT 10;
