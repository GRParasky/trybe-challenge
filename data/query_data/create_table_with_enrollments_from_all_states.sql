DROP TABLE IF EXISTS trybe_db.matriculas;

CREATE TABLE IF NOT EXISTS trybe_db.matriculas 
	AS SELECT * FROM trybe_db.matricula_co UNION SELECT * from trybe_db.matricula_nordeste
										   UNION SELECT * from trybe_db.matricula_norte
										   UNION SELECT * from trybe_db.matricula_sudeste
										   UNION SELECT * from trybe_db.matricula_sul;
