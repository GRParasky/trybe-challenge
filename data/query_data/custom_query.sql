	-- This is a custom query for consult the tables of Enrollment provided officially by INEP;
    -- This query is built to be more readable, showing only core fields of the table;
    -- These fields are: 
    --     - ID_ALUNO;
    --     - ID_MATRICULA;
    --     - ID_TURMA;
    --     - NU_ANO_CENSO;
    --     - CO_UF;
    --     - CO_MUNICIPIO;
    --     - NO_MUNICIPIO;
    --     - NU_IDADE_REFERENCIA;
    --     - TP_SEXO;
    --     - TP_COR_RACA;
    --     - TP_NACIONALIDADE;
    --     - TP_ETAPA_ENSINO.
	
    -- These are the tables you can use for the query:
    --     - matricula_co;
    --     - matricula_nordeste;
    --     - matricula_norte;
    --     - matricula_sudeste;
    --     - matricula_sul;
    --     - matriculas
    

    -- TIP: Use this code if you need to load a large amount of data

	-- set global max_allowed_packet=1000*1024*1024;
	-- show variables like 'max_allowed_packet'


SELECT ID_ALUNO,
       ID_MATRICULA, 
       ID_TURMA, 
       NU_ANO_CENSO, 
       CO_UF,
       CO_MUNICIPIO,
       NO_MUNICIPIO,
       NU_IDADE_REFERENCIA, 
       TP_SEXO, 
       TP_COR_RACA, 
       TP_NACIONALIDADE, 
       TP_ETAPA_ENSINO FROM trybe_db.matricula_co;
