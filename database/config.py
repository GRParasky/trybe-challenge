import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import Session


engine = create_engine('mysql+mysqlconnector://root:root@localhost:3306/trybe_db')

session = Session(engine)
session.connection()
