import os
import pandas as pd
from zipfile import ZipFile


def return_zipfile_object(path: str) -> ZipFile:
    for root, dirs, files in os.walk(path):
        for file in files:
            if '.zip' in file:
                z = ZipFile(os.path.join(path, file))
    return z


def return_list_of_file_inside_zip_file(zip_file: ZipFile) -> list[str]:
    return [name.split('/')[-1] for name in zip_file.namelist()]


def show_files_inside_zip_file(files_list: list) -> None:
    print('----- FILES INSIDE .ZIP FILE -----')
    for file in files_list:
        print(file) if file else ''


def extract_specific_file_based_on_name_or_prefix(zip_file: ZipFile, final_path: str, file_name: str = None,
                                                  report='Enrollment', prefix: str = None) -> None:
    for name in zip_file.namelist():
        if file_name == name.split('/')[-1] or prefix in name:
            if os.path.isdir(final_path):
                pass
            else:
                os.mkdir(final_path)
            zip_file.extract(name, os.path.join(final_path, report))


def return_list_of_files_inside_specific_folder(path: str, report: str = 'Enrollment'):
    files_list = list()
    for dirs, dir_names, files in os.walk(os.path.join(path, report)):
        for file in files:
            files_list.append(file)
    return files_list


def transform_file_into_pandas_dataframe(path, file, report='Enrollment', size=None, delimiter='|'):
    if '.csv' in file or '.CSV' in file:
        df = pd.read_csv(os.path.join(path, report, file), delimiter=delimiter)
    size = len(df) if not size else size
    df = df.head(size)
    return df


def send_pandas_dataframe_to_database(df: pd.DataFrame, engine, table_name, if_exists='replace', index=True):
    df.to_sql(table_name, engine, if_exists=if_exists, index=index)
